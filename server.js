const express = require('express');
const path = require('path');
const cors = require('cors');
const socket = require('socket.io');
const mongoose = require('mongoose');
const helmet = require('helmet');
const app = express();
const PORT = process.env.PORT || 8000;

const testimonialsRoutes = require('./routes/testimonials.routes');
const concertsRoutes = require('./routes/concerts.routes');
const seatsRoutes = require('./routes/seats.routes');

const server = app.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`);
});
const io = socket(server, {cors:{
  origin: "http://localhost:3000",
  methods: "GET,POST,PUT,DELETE"
}});

io.on('connection', (socket) => {
  console.log("New connection! Id: "+socket.id)
});

app.use(helmet());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors({
  origin: "http://localhost:3000",
  methods: "GET,POST,PUT,DELETE"
}))

app.use((req, res, next) => {
  req.io = io;
  next();
});

app.use('/api', testimonialsRoutes);
app.use('/api', concertsRoutes);
app.use('/api', seatsRoutes);
app.use(express.static(path.join(__dirname, '/client/build')));

const dbURI = process.env.NODE_ENV === 'production'
  ? `mongodb+srv://krzysieq:${process.env.DB_PASS}@cluster0.l0vpu39.mongodb.net/NewWaveDB?retryWrites=true&w=majority`
  : 'mongodb://localhost:27017/NewWaveDB';

mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

db.once('open', () => {
  console.log('Connected to the database');
});
db.on('error', err => console.log('Error ' + err));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/client/build/index.html'));
});

module.exports = server;