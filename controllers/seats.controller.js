const Seat = require('../models/seat.model');
const sanitize = require('mongo-sanitize');

exports.getAll = async (req, res) => {
  try {
    res.json(await Seat.find());
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.getById = async (req, res) => {
  try {
    const obj = await Seat.findById(req.params.id);
    if(!obj) res.status(404).json({ message: 'Not found' });
    else res.json(obj);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.create = async (req, res) => {
  try {
    const { day, seat, client, email } = req.body;
    const cleanClient = sanitize(client);
    const cleanEmail = sanitize(email);
    const newSeat = new Seat({ day, seat, client: cleanClient, email: cleanEmail });
    await newSeat.save();
    res.json({ message: 'OK' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.edit = async (req, res) => {
  try {
    const { day, seat, client, email } = req.body;
    const cleanClient = sanitize(client);
    const cleanEmail = sanitize(email);
    const obj = Seat.findById(req.params.id);
    if(obj) {
      await Seat.updateOne({ _id: req.params.id }, { $set: { day, seat, client: cleanClient, email: cleanEmail }})
      const updated = Seat.findById(req.params.id);
      res.json(updated);
    }
    else res.status(404).json({ message: 'Not found' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.delete = async (req, res) => {
  try {
    const obj = await Seat.findById(req.params.id);
    if(obj) {
      await Seat.deleteOne({ _id: req.params.id });
      res.json(obj);
    }
    else res.status(404).json({ message: 'Not found' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};