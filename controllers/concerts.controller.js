const Concert = require('../models/concert.model');
const Seat = require('../models/seat.model');
const sanitize = require('mongo-sanitize');

exports.getAll = async (req, res) => {
  try {
    const obj = await Concert.find();
    const data = [];
    for(let concert of obj) {
      const seatObj = await Seat.find({ day: concert.day });
      data.push({...concert._doc, tickets: 50 - seatObj.length});
    }
    res.json(data);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.getById = async (req, res) => {
  try {
    const obj = await Concert.findById(req.params.id);
    if(!obj) res.status(404).json({ message: 'Not found' });
    else res.json(obj);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.create = async (req, res) => {
  try {
    const { performer, genre, price, day, image } = req.body;
    const clean = sanitize(performer);
    const newConcert = new Concert({ performer: clean, genre, price, day, image });
    await newConcert.save();
    res.json({ message: 'OK' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.edit = async (req, res) => {
  try {
    const { performer, genre, price, day, image } = req.body;
    const clean = sanitize(performer);
    const obj = await Concert.findById(req.params.id);
    if(obj) {
      await Concert.updateOne({ _id: req.params.id }, { $set: { performer: clean, genre, price, day, image }});
      const updated = await Concert.findById(req.params.id);
      res.json(updated);
    }
    else res.status(404).json({ message: 'Not found' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.delete = async (req, res) => {
  try {
    const obj = await Concert.findById(req.params.id);
    if(obj) {
      await Concert.deleteOne({ _id: req.params.id });
      res.json(obj);
    }
    else res.status(404).json({ message: 'Not found' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.getByPerformer = async (req, res) => {
  try {
    const obj = await Concert.find({ performer: req.params.performer });
    if(!obj) res.status(404).json({ message: 'Not found' });
    else res.json(obj);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.getByGenre = async (req, res) => {
  try {
    const obj = await Concert.find({ genre: req.params.genre });
    if(!obj) res.status(404).json({ message: 'Not found' });
    else res.json(obj);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.getByPriceMinMax = async (req, res) => {
  try {
    const obj = await Concert.find({ price: { $gte: req.params.price_min, $lte: req.params.price_max} });
    if(!obj) res.status(404).json({ message: 'Not found' });
    else res.json(obj);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.getByDay = async (req, res) => {
  try {
    const obj = await Concert.find({ day: req.params.day });
    if(!obj) res.status(404).json({ message: 'Not found' });
    else res.json(obj);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};