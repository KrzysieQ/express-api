const Testimonial = require('../models/testimonial.model');
const sanitize = require('mongo-sanitize');

exports.getAll = async (req, res) => {
  try {
    res.json(await Testimonial.find());
  } catch(err) {
    res.status(500).json({ message: err });
  }
  res.json(db.testimonials);
};

exports.getRandom = async (req, res) => {
  try {
    const count = await Testimonial.countDocuments();
    const rand = Math.floor(Math.random() * count);
    const obj = await Testimonial.findOne().skip(rand);
    if(!obj) res.status(404).json({ message: 'Not found' });
    else res.json(obj);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.getById = async (req, res) => {
  try {
    const obj = await Testimonial.findById(req.params.id);
    if(!obj) res.status(404).json({ message: 'Not found' });
    else res.json(obj);
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.create = async (req, res) => {
  try {
    const { author, text } = req.body;
    const cleanAuthor = sanitize(author);
    const cleanText = sanitize(text);
    if(!author && !text) return res.status(400).json({ message: 'Invalid params' });

    const newTestimonial = new Testimonial({ author: cleanAuthor, text: cleanText });
    await res.json({ message: 'OK' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.edit = async (req, res) => {
  try {
    const { author, text } = req.body;
    const cleanAuthor = sanitize(author);
    const cleanText = sanitize(text);
    if(!author && !text) return res.status(400).json({ message: 'Invalid params' });

    const obj = await Testimonial.findById(req.params.id);
    if(obj) {
      await Testimonial.updateOne({ _id: req.params.id }, { $set: { author: cleanAuthor, text: cleanText }});
      const updated = await Testimonial.findById(req.params.id);
      res.json(updated);
    }
    else res.status(404).json({ message: 'Not found' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};

exports.delete = async (req, res) => {
  try {
    const obj = await Testimonial.findById(req.params.id);
    if(obj) {
      await Testimonial.deleteOne({ _id: req.params.id });
      res.json(obj);
    }
    else res.status(404).json({ message: 'Not found' });
  } catch(err) {
    res.status(500).json({ message: err });
  }
};